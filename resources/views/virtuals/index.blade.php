@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">List Virtual Gantry</div>
                    @include('notif')
                    <div class ="card-body">
                        <th><a href=" {{ route('virtuals.create') }}" class="btn btn-primary btn-sm" ><i class="fas fa-plus-circle"></i> Create New Virtual Gantry</a></th>
                    </div>
                        <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Gate Info</th>
                                    <th>Virtual Gate Code</th>
                                    <th>Virtual Gantry</th>
                                    <th>Virtual Gate Type</th>
                                    <th>V Address</th>
                                    <th>Lattitude</th>
                                    <th>Langitude</th>
                                    <th>Radius</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $virtuals as $virtual )
                                <tr>
                                    <th>{{ $loop->iteration }}</th>
                                    <th>{{ $virtual->gate_info }}</th>
                                    <th>{{ $virtual->vg_code }}</th>
                                    <th>{{ $virtual->virtual_gantry }}</th>
                                    <th>{{ $virtual->vg_type }}</th>
                                    <th>{{ $virtual->v_address }}</th>
                                    <th>{{ $virtual->langitude }}</th>
                                    <th>{{ $virtual->longitude }}</th>
                                    <th>{{ $virtual->radius }}</th>


                                    <td>
                                        {!! Form::open(['route'=>['virtuals.destroy',$virtual->id],'method'=>'DELETE']) !!}
                                        <a href="{{ route('virtuals.show',$virtual->id) }}" class="btn"><i class="fas fa-info-circle"></i></a>
                                        <a href=" {{ route('virtuals.edit',$virtual->id) }}" class="btn"><i class="fas fa-edit"></i></a>
                                        <button class="btn" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fas fa-minus-circle"></i></button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
$('#users-table').DataTable();
});
</script>
@endpush


