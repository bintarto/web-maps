@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Create Gate</div>
                    <div class="card-body">
                        @include('validate')
                        {!! Form::open(['route'=>'gates.store','method'=>'post']) !!}

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Gate Name</label>
                            <div class="col-md-4">
                                {!! Form::text('list_name', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Gate Address</label>
                            <div class="col-md-4">
                                {!! Form::text('list_adress', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Lang</label>
                            <div class="col-md-4">
                                {!! Form::text('langitude', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Long</label>
                            <div class="col-md-4">
                                {!! Form::text('longitude', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-secondary btn-sm">add</button>
                                <a href=" {{ route('gates.index') }}" class="btn btn-secondary btn-sm">back</a>
                            </div>
                        </div>
                                <div class="card-header">Our Location</div>
                                <div id="map"></div>
                                <div class="content"></div>
                                <div style="width: 1000px; height: 500px;">
                                    {!! Mapper::render() !!}
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    {{-- <script>
        // Initialize and add the map
        function initMap()
        {
          // The location of Indonesia
          var Indonesia = {lat: -6.200000	,  lng: 106.816666};
          // The map, centered at Indonesia
          var map = new google.maps.Map(
              document.getElementById('map'), {zoom: 8, center: indonesia});
          // The marker, positioned at Indonesia
          var marker = new google.maps.Marker ({position: indonesia, map: map});
        }
            </script>
            <!--Load the API from the specified URL
            * The async attribute allows the browser to render the page while the API loads
            * The key parameter will contain your own API key (which is not needed for this tutorial)
            * The callback parameter executes the initMap() function
            -->
            <script
            {{-- async defer --}}
            {{-- <div class="content">
                <div style="width: 1000px; height: 500px;">
                    {!! Mapper::render() !!}
                </div>
            </div> --}}
            {{-- src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMyjXZUcSAiFz2j_qf7K1HLof1tgl_RsU&libraries=places&language=id">
           src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMyjXZUcSAiFz2j_qf7K1HLof1tgl_RsU&callback=initMap">
    </script> --}} 

@endsection




