@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">List Gates</div>
                    @include('notif')
                    <div class ="card-body">
                        <th><a href=" {{ route('gates.create') }}" class="btn btn-primary btn-sm" ><i class="fas fa-plus-circle"></i> Create New Gates</a></th>
                    </div>
                        <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Branch Code</th>
                                    <th>Gate Name</th>
                                    <th>Gate Code</th>
                                    <th>Gate Adress</th>
                                    <th>Lat</th>
                                    <th>Long</th>
                                    <th>Radius</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $gates as $gate )
                                <tr>
                                    <th>{{ $loop->iteration }}</th>
                                    <th>{{ $gate->branch_info }}</th>
                                    <th>{{ $gate->gate_name }}</th>
                                    <th>{{ $gate->gate_code }}</th>
                                    <th>{{ $gate->list_adress }}</th>
                                    <th>{{ $gate->langitude }}</th>
                                    <th>{{ $gate->longitude }}</th>
                                    <th>{{ $gate->radius }}</th>
                                    <td>
                                        {!! Form::open(['route'=>['gates.destroy',$gate->id],'method'=>'DELETE']) !!}
                                        <a href="{{ route('gates.show',$gate->id) }}" class="btn"><i class="fas fa-info-circle"></i></a>
                                        <a href=" {{ route('gates.edit',$gate->id) }}" class="btn"><i class="fas fa-edit"></i></a>
                                        <button class="btn" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fas fa-minus-circle"></i></button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
$('#users-table').DataTable();
});
</script>
@endpush


