@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">List Branch</div>
                    @include('notif')
                    <div class ="card-body">
                        <th><a href=" {{ route('branches.create') }}" class="btn btn-primary btn-sm" ><i class="fas fa-plus-circle"></i> Create New Branch</a></th>
                    </div>
                        <table class="table table-bordered" id="users-table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    {{-- <th>Branch ID</th> --}}
                                    <th>Branch Code</th>
                                    <th>Branch Name</th>
                                    {{-- <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>--}}
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ( $branches as $branch )
                                <tr>
                                    <th>{{ $loop->iteration }}</th>
                                    {{-- <th>{{ $branch->branch_id }}</th> --}}
                                    <th>{{ $branch->branch_code }}</th>
                                    <th>{{ $branch->branch_name }}</th>
                                    {{-- <th>{{ $branch->phone }}</th>
                                    <th>{{ $branch->email }}</th>
                                    <th>{{ $branch->address }}</th> --}}
                                    <td>
                                        {!! Form::open(['route'=>['branches.destroy',$branch->id],'method'=>'DELETE']) !!}
                                        <a href="{{ route('branches.show',$branch->id) }}" class="btn"><i class="fas fa-info-circle"></i></a>
                                        <a href=" {{ route('branches.edit',$branch->id) }}" class="btn"><i class="fas fa-edit"></i></a>
                                        <button class="btn" type="submit" onclick="return confirm('Yakin ingin menghapus data?')"><i class="fas fa-minus-circle"></i></button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
$(function() {
$('#users-table').DataTable();
});
</script>
@endpush


