@extends('layouts.app')

@section('content')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMyjXZUcSAiFz2j_qf7K1HLof1tgl_RsU&libraries=places&language=id"></script>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1 class="mt-5">Detail Gates</h1>

            <div class="card">
                    <div class="card-body">
                    <p class="card-text">{{ $gate->gate_name }}</p>
                    <h5 class="card-title">{{ $gate->gate_code }}</h5>
                    <h6 class="card-subtitle mb-2 text-muted">{{ $gate->list_adress }}</h6>
                    <p class="card-text">{{ $gate->langitude }}</p>
                    <p class="card-text">{{ $gate->longitude }}</p>



                    <div class="card-header">Our Location</div>
                    <div id="map" style="height: 500px; width:100%;"></div>
                    {{-- <div style="width: 1000px; height: 500px;"> --}}
                        {{-- {!! Mapper::render() !!} --}}
                    </div>

                    {{-- <a href="{{ $gate->id }}/edit" class="btn btn-primary btn-sm">edit</a>
                    <form action="/gate/{{ $gate->id }}" method="post" class="d-inline">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-danger btn-sm">delete</button>
                    </form> --}}
                    <a href="/gates" class="card-link btn-sm "><i class="fas fa-undo-alt"></i> back</a>
                    <td>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customjs')
    <script>
        var markers = [];
        var marker;
        var cityCircle;
        var cityCircles = [];

        function initialize() {
            geocoder = new google.maps.Geocoder();
            var mapOptions = {
                center: new google.maps.LatLng({{$gate->langitude}}, {{$gate->longitude}}),
                zoom: 15
            };
            var myLatLng = {lat: {{$gate->langitude}}, lng: {{$gate->longitude}} };
            map = new google.maps.Map(document.getElementById("map"),
                    mapOptions);

            var input = document.getElementById('autocomplete');

            var options = {
                types: ['address']
            };
            autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.bindTo('bounds', map);
            var infowindow = new google.maps.InfoWindow();
            @foreach($gate['distance'] as $flyers)

            var myLatLngs = {lat: {{$flyers->langitude}}, lng: {{$flyers->longitude}} };
                markers = new google.maps.Marker({
                        map: map,
                        draggable: false,
                        position: new google.maps.LatLng(myLatLngs),
                        anchorPoint: new google.maps.Point(0, -29)
                });

            @endforeach
            cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                center:  myLatLng,
                radius: Math.sqrt(5) * 100
            });
            marker = new google.maps.Marker({
                        map: map,
                        draggable: false,
                        position: new google.maps.LatLng(myLatLng),
                        anchorPoint: new google.maps.Point(0, -29)
                });

            // fungsi ketika marekr di gerakan mengambil alamat lengkap dan di isikan di text box alamat
            google.maps.event.addListener(marker, 'dragend', function (event) {


                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    // bersihkan dahulu isi dari textbox karena akan di ganti menjadi yang terbaru
                    $('#langitude').val('');
                    $('#longitude').val('');
                    $('#autocomplete').val('');

                    // sesudah di bersihkan baru di isi kembali oleh value berikut
                    var marker_positions = marker.getPosition();

                    $('#langitude').val(marker_positions.lat());
                    $('#longitude').val(marker_positions.lng());
                    $('#autocomplete').val(results[0].formatted_address);
                    cityCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center:  marker_positions,
                        radius: Math.sqrt(5) * 100
                    });
                    cityCircles.push(cityCircle);

                // tidak lupa untuk menampilkan label alamat tepat di atas markernya
                var address = '';
                    if (results[0].address_components) {
                        address = [
                            (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                            (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                            (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                        ].join(' ');
                    }
                    infowindow.setContent('<div><strong>' + address + '</strong>');
                    infowindow.open(map, marker);
                })
            });

            autocomplete.addListener('place_changed', function () {
                  // hapus marker
                deleteMarkers();

                var place = this.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                $('#langitude').val(place.geometry.location.lat());
                $('#longitude').val(place.geometry.location.lng());
                map.setCenter(place.geometry.location);//center the map over the result

                    // if marker exists and has a .setMap method, hide it or clear old marker
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }

                marker = new google.maps.Marker(
                        {
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            position: place.geometry.location
                        });

                markers.push(marker);
                cityCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center:  place.geometry.location,
                        radius: Math.sqrt(5) * 100
                    });
                    cityCircles.push(cityCircle);

                    // fungsi ketika marekr di gerakan mengambil alamat lengkap dan di isikan di text box alamat
                google.maps.event.addListener(marker, 'dragend', function (event) {
                geocoder.geocode({
                'latLng': event.latLng
                }, function(results, status) {
                // bersihkan dahulu isi dari textbox karena akan di ganti menjadi yang terbaru
                $('#langitude').val('');
                $('#longitude').val('');
                $('#autocomplete').val('');

              // sesudah di bersihkan baru di isi kembali oleh value berikut
                var marker_positions = marker.getPosition();

                $('#langitude').val(marker_positions.lat());
                $('#longitude').val(marker_positions.lng());
                $('#autocomplete').val(results[0].formatted_address);

              // tidak lupa untuk menampilkan label alamat tepat di atas markernya
              var address = '';
                if (results[0].address_components) {
                    address = [
                        (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                        (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                        (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                    ].join(' ');
                }
                infowindow.setContent('<div><strong>' + address + '</strong>');
                infowindow.open(map, marker);
              })
                  });

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>
@endsection
