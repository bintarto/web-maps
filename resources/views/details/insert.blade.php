@extends('layouts.app')

@section('content')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMyjXZUcSAiFz2j_qf7K1HLof1tgl_RsU&libraries=places&language=id"></script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">M L F F</div>
                    <div class="card-body">
                        @include('validate')
                        {!! Form::open(['route'=>'details.store','method'=>'post']) !!}

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right" for="branch_info"> Branch Info </label>
                            <div class="col-md-4">
                                <select class="form-control" id="branch_info" name="branch_info" data-placeholder="Select Roads">
                                    <option></option>
                                    @foreach ($data['filtergate'] as $gate)
                                        <option value="{{$gate->branch_code}}" >{{$gate->branch_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right" for="gate_info"> Gate Info </label>
                            <div class="col-md-4">
                                <select class="form-control" id="gate_info" name="gate_info" data-placeholder="Select Roads">
                                    <option></option>
                                </select>
                            </div>
                        </div>

                        {{-- <div class="form-group row" id="gate_code">
                            <label class="col-md-2 col-form-label text-md-right" for="kode_gate"> Gate Code </label>
                            <div class="col-md-4">
                            <input type="text" name="gate_code" id="gate_code" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row" id="gate_name">
                            <label class="col-md-2 col-form-label text-md-right" for="gate_name"> Gate Name  </label>
                            <div class="col-md-4">
                            <input type="text" name="gate_name" id="gate_name" class="form-control">
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            <label for="autocomplete" class="col-md-2 col-form-label text-md-right">Gate Address</label>
                            <div class="col-md-4">
                                {!! Form::text('autocomplete', null, ['class'=>'form-control' ], ) !!}
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right" for="autocomplete"> Gate Address </label>
                            <div class="col-md-4">
                            <input type="text" name="list_adress" id="autocomplete" class="form-control" placeholder="Select Location">
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Lat</label>
                            <div class="col-md-4">
                                {!! Form::text('langitude', null, ['class'=>'form-control']) !!}
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row" id="lat_area">
                            <label class="col-md-2 col-form-label text-md-right" for="langitude"> Latitude </label>
                            <div class="col-md-4">
                            <input type="text" name="langitude" id="langitude" class="form-control">
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row">
                            <label class="col-md-2 col-form-label text-md-right">Long</label>
                            <div class="col-md-4">
                                {!! Form::text('longitude', null, ['class'=>'form-control']) !!}
                            </div>
                        </div> --}}

                        {{-- <div class="form-group row" id="long_area">
                            <label class="col-md-2 col-form-label text-md-right" for="latitude"> Longitude </label>
                            <div class="col-md-4">
                            <input type="text" name="longitude" id="longitude" class="form-control">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-2">
                                <button type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-user-plus"></i> add</button>
                                <a href=" {{ route('gates.index') }}" class="btn btn-secondary btn-sm"><i class="fas fa-undo-alt"></i> back</a>
                            </div>
                        </div> --}}

                            <div class="card-header">Our Location</div>
                             <div>
                                <div id="map" style="width:100%;height:380px;"></div>
                                {{-- <div style="width: 1000px; height: 500px;"> --}}
                                    {{-- {!! Mapper::render() !!} --}}

                             </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('customjs')
    <script>

	$(document).ready(function()
	{
		$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
		});
		var markers = [];
		var gate =[];
		var virtualgate =[];
		var marker;
		var cityCircle;
        var cityCircles = [];

		$('#branch_info').on('change', function() {
			$("#gate_info").empty();
			gate = [];
			marker.setMap(null);
			var branch_info = $(this).val();
            var url2 ="{{ url('details/gate_info/') }}";
			url = url2 + '/' + branch_info;
			$.ajax({
				type: 'GET',
				url: url,
				success: function(response) {
					$("#gate_info").append(''
					+'<option value="" disabled selected>{{ __('Choose Gate Info') }}</option>'
					);

					$.each(response, function (index, data) {

						gate.push([data.langitude,data.longitude, data.gate_name, data.gate_code])
						$("#gate_info").append(''
											+'		@if (old('gate_info') == '+data.branch_info+')'
											+'			<option value="'+data.gate_code+'" selected>'+data.gate_name+'</option>'
											+'		@else'
											+'			<option value="'+data.gate_code+'">'+data.gate_name+'</option>'
											+'		@endif');



                    });

					changemarkerbranch();

				},
				error: function(obj, response) {
					$("#main_message").html(response);
				}
			});
		});

		$('#gate_info').on('change',function(){
			marker.setMap(null);
			virtualgate = [];
			gate = [];
			cityCircle.setMap(null);
            var gate_info = $(this).val();
			var url2 ="{{ url('details/gate_maps/') }}";
            url = url2 + '/' + gate_info;
			$.ajax({
				type: 'GET',
				url: url,
				success: function(response) {
                    $.each(response, function (index, data) {

						virtualgate.push([data.langitude,data.longitude,data.vg_code]) ;


				        changemarkerbranch();
                        changemarkervg();
                    });


				},
				error: function(obj, response) {
                    $("#main_message").html(response);

				}
			});

		});

		function changemarkerbranch(){
            marker.setMap(null);
            cityCircle.setMap(null);
			for (i  = 0; i < gate.length; i++){
				var myLatLngs = {lat: parseFloat(gate[i][0]), lng: parseFloat(gate[i][1])	 };
                var contentString = gate[i][2]+' - '+gate[i][3];
                var infowindow = new google.maps.InfoWindow({
                    content: contentString,
                    position: myLatLngs
                });
                marker = new google.maps.Marker({
                        map: map,
                        draggable: false,
                        position: new google.maps.LatLng(myLatLngs),
                        anchorPoint: new google.maps.Point(0, -29)
                });

                cityCircle = new google.maps.Circle({
					strokeColor: '#0000FF',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#0000FF',
					fillOpacity: 0.35,
					map: map,
					center:  myLatLngs,
					radius: Math.sqrt(5) * 100
                });
			}
            marker.addListener('click', function() {
            // tampilkan info window di atas marker
                infowindow.open(map, marker);
            });
            cityCircles.push(cityCircle);
			map.setCenter(myLatLngs);
			markers.push(marker);
		}
		function changemarkervg(){
			for (i  < 0; i < virtualgate.length; i++){
				var myLatLngs2 = {lat: parseFloat(virtualgate[i][0]), lng: parseFloat(virtualgate[i][1])};
                var contentString = virtualgate[i][2];
                var infowindow = new google.maps.InfoWindow()
                marker = new google.maps.Marker({
                        map: map,
                        draggable: false,
                        position: new google.maps.LatLng(myLatLngs2),
                        anchorPoint: new google.maps.Point(0, -29)
                });

                google.maps.event.addListener(marker,'click', (function(marker,contentString,infowindow){
                return function() {
                    infowindow.setContent(contentString);
                    infowindow.open(map,marker);
                    };
                })(marker,contentString,infowindow));

                map.setCenter(myLatLngs2);
			    markers.push(marker);

				cityCircle = new google.maps.Circle({
					strokeColor: '#0000FF',
					strokeOpacity: 0.8,
					strokeWeight: 2,
					fillColor: '#0000FF',
					fillOpacity: 0.35,
					map: map,
					center:  myLatLngs2,
					radius: Math.sqrt(5) * 100
                });

			}

			cityCircles.push(cityCircle);

		}
		function initialize() {

            geocoder = new google.maps.Geocoder();
            var mapOptions = {
                center: new google.maps.LatLng(-6.902414332507406, 107.61862761201178),
                zoom: 15,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };


            map = new google.maps.Map(document.getElementById("map"),
                    mapOptions);

            var input = document.getElementById('autocomplete');

            var options = {
                types: ['address']
            };
            autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.bindTo('bounds', map);
            var infowindow = new google.maps.InfoWindow();

            marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: new google.maps.LatLng(-6.902414332507406, 107.61862761201178),
                anchorPoint: new google.maps.Point(0, -29)
            });

            // fungsi ketika marekr di gerakan mengambil alamat lengkap dan di isikan di text box alamat
            google.maps.event.addListener(marker, 'dragend', function (event) {
                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    // bersihkan dahulu isi dari textbox karena akan di ganti menjadi yang terbaru
                    $('#langitude').val('');
                    $('#longitude').val('');
                    $('#autocomplete').val('');
                    cityCircle.setMap(null);

                    // sesudah di bersihkan baru di isi kembali oleh value berikut
                    var marker_positions = marker.getPosition();

                    $('#langitude').val(marker_positions.lat());
                    $('#longitude').val(marker_positions.lng());
                    $('#autocomplete').val(results[0].formatted_address);

                        cityCircle = new google.maps.Circle({
                            strokeColor: '##0000FF',
                            strokeOpacity: 0.8,
                            strokeWeight: 2,
                            fillColor: '#0000FF',
                            fillOpacity: 0.35,
                            map: map,
                            center:  marker_positions,
                            radius: Math.sqrt(5) * 100
                        });
                        cityCircles.push(cityCircle);

                // tidak lupa untuk menampilkan label alamat tepat di atas markernya
                var address = '';
                    if (results[0].address_components) {
                        address = [
                            (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                            (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                            (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                        ].join(' ');
                    }
                    infowindow.setContent('<div><strong>' + address + '</strong>');
                    infowindow.open(map, marker);
                })
            });

                    cityCircle = new google.maps.Circle({
                    strokeColor: '#0000FF',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#0000FF',
                    fillOpacity: 0.35,
                    map: map,
                    center:  {lat:$('#langitude').val(), lng:$('#longitude').val()},
                    radius: Math.sqrt(10000) * 100
                });
                autocomplete.addListener('place_changed', function () {
                    var place = autocomplete.getPlace();
                        if (!place.geometry) {
                            window.alert("Autocomplete's returned place contains no geometry");
                            return;
                        }

                    // If the place has a geometry, then present it on a map.
                    if (place.geometry.viewport) {
                        map.fitBounds(place.geometry.viewport);
                    } else {
                        map.setCenter(place.geometry.location);
                        map.setZoom(17);
                    }

                    $('#langitude').val(place.geometry.location.lat());
                    $('#longitude').val(place.geometry.location.lng());


                    map.setCenter(place.geometry.location);//center the map over the result

                    marker = new google.maps.Marker(
                            {
                                map: map,
                                draggable: true,
                                animation: google.maps.Animation.DROP,
                                position: place.geometry.location
                            });

                    markers.push(marker);
                    cityCircle = new google.maps.Circle({
                        strokeColor: '#0000FF',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#0000FF',
                        fillOpacity: 0.35,
                        map: map,
                        center:  place.geometry.location,
                        radius: Math.sqrt(5) * 10
                    });
                    cityCircles.push(cityCircle);
                    // fungsi ketika marker di gerakan mengambil alamat lengkap dan di isikan di text box alamat
                    google.maps.event.addListener(marker, 'dragend', function (event) {
                        geocoder.geocode({
                            'latLng': event.latLng
                        }, function(results, status) {
                        // bersihkan dahulu isi dari textbox karena akan di ganti menjadi yang terbaru
                        $('#langitude').val('');
                        $('#longitude').val('');
                        $('#autocomplete').val('');

                        // sesudah di bersihkan baru di isi kembali oleh value berikut
                        var marker_positions = marker.getPosition();
                                $('#langitude').val(marker_positions.lat());
                                $('#longitude').val(marker_positions.lng());

                        $('#autocomplete').val(results[0].formatted_address);

                     // tidak lupa untuk menampilkan label alamat tepat di atas markernya
                            var address = '';
                            if (results[0].address_components) {
                                address = [
                                    (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                                    (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                                    (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                                ].join(' ');
                            }
                            infowindow.setContent('<div><strong>' + address + '</strong>');
                            infowindow.open(map, marker);
                })
                    });

                    var address = '';
                    if (place.address_components) {
                        address = [
                            (place.address_components[0] && place.address_components[0].short_name || ''),
                            (place.address_components[1] && place.address_components[1].short_name || ''),
                            (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                    }

                    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                    infowindow.open(map, marker);
                });

        }


        google.maps.event.addDomListener(window, 'load', initialize);


	});






    </script>
@endsection
