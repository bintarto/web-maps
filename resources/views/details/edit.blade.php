@extends('layouts.app')

@section('content')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMyjXZUcSAiFz2j_qf7K1HLof1tgl_RsU&libraries=places&language=id"></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Edit Gates</div>
                      <div class="card-body">
                          @include('validate')
                        {!! Form::model($gates,['route'=>['gates.update',$gates->id],'method'=>'put']) !!}

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Branch Info</label>
                                <div class="col-md-4">

                                    <select {!! Form::text('branch_info', null, ['class'=>'form-control']) !!} data-placeholder="Select Branch">
                                        <option></option>
                                        @foreach ($filterbranch as $branch)
                                            <option value="{{$branch->branch_code}}-{{$branch->branch_name}}" >{{$branch->branch_code.'-'.$branch->branch_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">Gate Code</label>
                                    <div class="col-md-4">
                                      {!! Form::text('gate_code', null, ['class'=>'form-control']) !!}
                                    </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right">Gate Name</label>
                                <div class="col-md-4">
                                  {!! Form::text('gate_name', null, ['class'=>'form-control']) !!}
                                </div>
                        </div>

                            <div class="form-group row">
                                <label class="col-md-2 col-form-label text-md-right" for="autocomplete"> Gate Address </label>
                                <div class="col-md-4">
                                {{-- <input type="text" name="list_adress" id="autocomplete" class="form-control" placeholder="Select Location"> --}}

                                {!! Form::text('list_adress', null, ['class'=>'form-control','id'=> 'autocomplete']) !!}
                                </div>
                            </div>
                            {{-- <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">Gate Address</label>
                                    <div class="col-md-4">
                                      {!! Form::text('list_adress', null, ['class'=>'form-control']) !!}
                                    </div>
                            </div> --}}
                            {{-- <div class="form-group row" id="lat_area">
                                <label class="col-md-2 col-form-label text-md-right" for="langitude"> Latitude </label>
                                <div class="col-md-4">
                                <input type="text" name="langitude" id="langitude" class="form-control">
                                </div>
                            </div> --}}
                             <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">Lang</label>
                                    <div class="col-md-4">
                                      {!! Form::text('langitude', null, ['class'=>'form-control','id'=> 'langitude']) !!}
                                    </div>
                            </div>
                            {{-- <div class="form-group row" id="long_area">
                                <label class="col-md-2 col-form-label text-md-right" for="latitude"> Longitude </label>
                                <div class="col-md-4">
                                <input type="text" name="longitude" id="longitude" class="form-control">
                                </div>
                            </div> --}}
                            <div class="form-group row">
                                    <label class="col-md-2 col-form-label text-md-right">Long</label>
                                    <div class="col-md-4">
                                      {!! Form::text('longitude', null, ['class'=>'form-control','id'=> 'longitude']) !!}
                                    </div>
                            </div>

                            <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-2">
                                        <button type="submit" class="btn btn-secondary"><i class="far fa-edit"></i> update</button>
                                        <a href=" {{ route('gates.index') }}" class="btn btn-secondary"><i class="fas fa-undo-alt"></i> back</a>
                                    </div>
                            </div>

                            <div class="card-header">Our Location</div>
                            <div id="map" style="height: 500px; width:100%;"></div>
                            {{-- <div style="width: 1000px; height: 500px;"> --}}
                                {{-- {!! Mapper::render() !!} --}}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('customjs')
    <script>
        var markers = [];
        var marker;
        var cityCircle;
        var cityCircles = [];

        function initialize() {
            geocoder = new google.maps.Geocoder();
            var mapOptions = {
                center: new google.maps.LatLng({{$gates->langitude}}, {{$gates->longitude}}),
                zoom: 15
            };
            var myLatLng = {lat: {{$gates->langitude}}, lng: {{$gates->longitude}} };
            map = new google.maps.Map(document.getElementById("map"),
                    mapOptions);

            var input = document.getElementById('autocomplete');

            var options = {
                types: ['address']
            };
            autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.bindTo('bounds', map);
            var infowindow = new google.maps.InfoWindow();
            @foreach($gates['distance'] as $flyers)

            var myLatLngs = {lat: {{$flyers->langitude}}, lng: {{$flyers->longitude}} };
                markers = new google.maps.Marker({
                        map: map,
                        draggable: false,
                        position: new google.maps.LatLng(myLatLngs),
                        anchorPoint: new google.maps.Point(0, -29)
                });

            @endforeach
            cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: map,
                center:  myLatLng,
                radius: Math.sqrt(5) * 100
            });

            marker = new google.maps.Marker({
                        map: map,
                        draggable: true,
                        position: new google.maps.LatLng(myLatLng),
                        anchorPoint: new google.maps.Point(0, -29)
                });

            // fungsi ketika marekr di gerakan mengambil alamat lengkap dan di isikan di text box alamat
            google.maps.event.addListener(marker, 'dragend', function (event) {


                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    // bersihkan dahulu isi dari textbox karena akan di ganti menjadi yang terbaru
                    $('#langitude').val('');
                    $('#longitude').val('');
                    $('#autocomplete').val('');
                    cityCircle.setMap(null);

                    // sesudah di bersihkan baru di isi kembali oleh value berikut
                    var marker_positions = marker.getPosition();

                    $('#langitude').val(marker_positions.lat());
                    $('#longitude').val(marker_positions.lng());
                    $('#autocomplete').val(results[0].formatted_address);
                    cityCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center:  marker_positions,
                        radius: Math.sqrt(5) * 100
                    });
                    cityCircles.push(cityCircle);

                // tidak lupa untuk menampilkan label alamat tepat di atas markernya
                var address = '';
                    if (results[0].address_components) {
                        address = [
                            (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                            (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                            (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                        ].join(' ');
                    }
                    infowindow.setContent('<div><strong>' + address + '</strong>');
                    infowindow.open(map, marker);
                })
            });

            autocomplete.addListener('place_changed', function () {
                  // hapus marker
                deleteMarkers();

                var place = this.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);
                }

                $('#langitude').val(place.geometry.location.lat());
                $('#longitude').val(place.geometry.location.lng());
                map.setCenter(place.geometry.location);//center the map over the result

                    // if marker exists and has a .setMap method, hide it or clear old marker
                if (marker && marker.setMap) {
                    marker.setMap(null);
                }

                marker = new google.maps.Marker(
                        {
                            map: map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            position: place.geometry.location
                        });

                markers.push(marker);
                cityCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.35,
                        map: map,
                        center:  place.geometry.location,
                        radius: Math.sqrt(5) * 100
                    });
                    cityCircles.push(cityCircle);

                    // fungsi ketika marekr di gerakan mengambil alamat lengkap dan di isikan di text box alamat
                google.maps.event.addListener(marker, 'dragend', function (event) {
                geocoder.geocode({
                'latLng': event.latLng
                }, function(results, status) {
                // bersihkan dahulu isi dari textbox karena akan di ganti menjadi yang terbaru
                $('#langitude').val('');
                $('#longitude').val('');
                $('#autocomplete').val('');

              // sesudah di bersihkan baru di isi kembali oleh value berikut
                var marker_positions = marker.getPosition();

                $('#langitude').val(marker_positions.lat());
                $('#longitude').val(marker_positions.lng());
                $('#autocomplete').val(results[0].formatted_address);

              // tidak lupa untuk menampilkan label alamat tepat di atas markernya
              var address = '';
                if (results[0].address_components) {
                    address = [
                        (results[0].address_components[0] && results[0].address_components[0].short_name || ''),
                        (results[0].address_components[1] && results[0].address_components[1].short_name || ''),
                        (results[0].address_components[2] && results[0].address_components[2].short_name || '')
                    ].join(' ');
                }
                infowindow.setContent('<div><strong>' + address + '</strong>');
                infowindow.open(map, marker);
              })
                  });

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);


    </script>
@endsection
