<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;
use DB;


class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Mapper::map(-6.200000, 106.816666);

        $branches=Branch::all();
        return view('branches.index', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('branches.insertbranch');
    }
         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            // 'branch_id'=>'required',
            'branch_code'=>'required',
            'branch_name'=>'required'
            // 'phone'=>'required',
            // 'email'=>'required',
            // 'address'=>'required'
        ]);

        $branch=Branch::create($request->all());
        return redirect()->route('branches.index')->with('pesan', 'berhasil hore');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        // $lat=DB::select('call distance(?)',array(8700)
        // );
        // $gate['distance']=$lat;

        // $gate = Gate::select('id','list_name','langitude','longitude',
        //         DB::raw('( 3959 * ACOS(COS(RADIANS(37))
        //         * COS (RADIANS(langitude))
        //         * COS(RADIANS(longitude)
        //         - RADIANS(-122))
        //         + SIN(RADIANS(37))
        //         * SIN(RADIANS(langitude)) ) )) AS distance')
        //         )->get();


        // Mapper::map($gate->langitude, $gate->longitude, ['draggable' => false]);
        // Mapper::map(
        //     -6.200000, 106.816666,
        //     [
        //     'draggable' => true,
        //     'eventDragEnd' => 'test_js(this.getPosition().lat(), this.getPosition().lng());'
        //     ]);

        return view('branches.show', compact('branch'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branches=Branch::findOrfail($id);
        // $lat=DB::select('call distance(?)',array(8700)
        // );
        // $gates['distance']=$lat;
        // dd($gates['distance']);




        // Mapper::map(
        //     $gates->langitude, $gates->longitude,
        //     [
        //     'draggable' => true,
        //     'eventDragEnd' => 'test_js(this.getPosition().lat(), this.getPosition().lng());'
        //     ]);


        return view('branches.edit', compact('branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            // 'branch_id'=>'required',
            'branch_code'=>'required',
            'branch_name'=>'required'
            // 'phone'=>'required',
            // 'email'=>'required',
            // 'address'=>'required'
        ]);

        $branch=Branch::find($id);
        $branch->update($request->all());
        return redirect()->route('branches.index')->with('pesan', 'data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch=Branch::find($id);
        $branch->delete();
        return redirect()->route('branches.index')->with('pesan', 'data berhasil dihapus');

    }

}
