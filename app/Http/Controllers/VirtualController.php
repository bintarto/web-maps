<?php

namespace App\Http\Controllers;

use App\Gate;
use App\Virtual;
use Illuminate\Http\Request;
use DB;

class VirtualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $virtuals=Virtual::all();
        return view('virtuals.index', compact('virtuals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $data['filtergate'] = Gate::select('gate_code','gate_name')->get();
            //  dd($data['filtergate']);

        return view('virtuals.insertvirtuals',$data);
    }
         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'gate_info'=>'required',
            'vg_code'=>'required',
            'virtual_gantry'=>'required',
            'vg_type'=>'required',
            'v_address'=>'required',
            'langitude'=>'required',
            'longitude'=>'required'
        ]);

        $road=Virtual::create($request->all());
        return redirect()->route('virtuals.index')->with('pesan', 'berhasil hore');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function show(Virtual $virtual)
    {
        // $lat=DB::select('call distance(?)',array(8700)
        // );
        // $gate['distance']=$lat;
        $gates=DB::select('select * from public.distance()');

        // $gate = Gate::select('id','list_name','langitude','longitude',
        //         DB::raw('( 3959 * ACOS(COS(RADIANS(37))
        //         * COS (RADIANS(langitude))
        //         * COS(RADIANS(longitude)
        //         - RADIANS(-122))
        //         + SIN(RADIANS(37))
        //         * SIN(RADIANS(langitude)) ) )) AS distance')
        //         )->get();


        // Mapper::map($gate->langitude, $gate->longitude, ['draggable' => false]);
        // Mapper::map(
        //     -6.200000, 106.816666,
        //     [
        //     'draggable' => true,
        //     'eventDragEnd' => 'test_js(this.getPosition().lat(), this.getPosition().lng());'
        //     ]);

        return view('virtuals.show', compact('virtual'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $virtuals=Virtual::findOrfail($id);
        $data['filtergate'] = Gate::select('gate_code','gate_name')->get();


        // $lat=DB::select('call distance(?)',array(8700)
        // );
        // $gates['distance']=$lat;
        // dd($gates['distance']);


        return view('virtuals.edit', compact('virtuals'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'gate_info'=>'required',
            'vg_code'=>'required',
            'virtual_gantry'=>'required',
            'vg_type'=>'required',
            'v_address'=>'required',
            'langitude'=>'required',
            'longitude'=>'required'
        ]);

        $virtual=Virtual::find($id);
        $virtual->update($request->all());
        return redirect()->route('virtuals.index')->with('pesan', 'data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $virtual=Virtual::find($id);
        $virtual->delete();
        return redirect()->route('virtuals.index')->with('pesan', 'data berhasil dihapus');

    }

}
