<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cornford\Googlmapper\Facades\MapperFacade as Mapper;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //   Mapper::map(-6.200000, 106.816666);

        Mapper::map(-6.200000, 106.81666, ['draggable' => true, 'eventDragEnd' => 'alert("left click");']);
        // Mapper::map(-6.200000, 106.81666, ['animation' => 'DROP', 'label' => 'Marker', 'title' => 'Marker', 'draggable' => true]);
        // Mapper::map(-6.200000, 106.81666, ['icon' => 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=|FE6256|000000']);
        // Mapper::map(-6.200000, 106.81666, ['icon' => ['url' => 'https://chart.googleapis.com/chart?chst=d_map_pin_letter&chld=|FE6256|000000', 'scale' => 100]]);
        // Mapper::map(-6.200000, 106.81666, ['markers' => ['icon' => ['symbol' => 'CIRCLE', 'scale' => 10], 'animation' => 'DROP', 'label' => 'Marker', 'title' => 'Marker']])->marker(-6.200000, 106.81666);
        // Mapper::map(-6.200000, 106.81666, [
        //     'title' 	=> 'title',
        //     'icon'      => [
        //         'path'         => 'M10.5,0C4.7,0,0,4.7,0,10.5c0,10.2,9.8,19,10.2,19.4c0.1,0.1,0.2,0.1,0.3,0.1s0.2,0,0.3-0.1C11.2,29.5,21,20.7,21,10.5 C21,4.7,16.3,0,10.5,0z M10.5,5c3,0,5.5,2.5,5.5,5.5S13.5,16,10.5,16S5,13.5,5,10.5S7.5,5,10.5,5z',
        //         'fillColor'    => '#DD716C',
        //         'fillOpacity'  => 1,
        //         'strokeWeight' => 0,
        //         'anchor'       => [0, 0],
        //         'origin'       => [0, 0],
        //         'size'         => [21, 30]
        //     ],
        //     'label'     => [
        //         'text' => 'Marker',
        //         'color' => '#B9B9B9',
        //         'fontFamily' => 'Arial',
        //         'fontSize' => '13px',
        //         'fontWeight' => 'bold',
        //     ],
        //     'autoClose' => true,
        //     'clickable' => false,
        //     'cursor' => 'default',
        //     'opacity' => 0.5,
        //     'visible' => true,
        //     'zIndex' => 1000,
        // ]);

        return view('map');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
