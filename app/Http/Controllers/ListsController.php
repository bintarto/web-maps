<?php

namespace App\Http\Controllers;

use App\Gate;
use App\Branch;
use Illuminate\Http\Request;
use DB;


class ListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $gates=Gate::all();
        return view('gates.index', compact('gates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $data['filterbranch'] = Branch::select('branch_code','branch_name')->get();
        //  dd($data['filterbranch']);

        return view('gates.insert',$data);
    }
         /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'branch_info'=>'required',
            'gate_name'=>'required',
            'gate_code'=>'required',
            'list_adress'=>'required',
            'langitude'=>'required',
            'longitude'=>'required',
            'radius'=>'required'

        ]);

        $gate=Gate::create($request->all());
        return redirect()->route('gates.index')->with('pesan', 'berhasil hore');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function show(Gate $gate)
    {
        // $lat=DB::select('call distance(?)',array(8700)
        // );
        // $gate['distance']=$lat;

        $gate['distance']=DB::select('select * from public.distance()');

        // $gate = Gate::select('id','list_name','langitude','longitude',
        //         DB::raw('( 3959 * ACOS(COS(RADIANS(37))
        //         * COS (RADIANS(langitude))
        //         * COS(RADIANS(longitude)
        //         - RADIANS(-122))
        //         + SIN(RADIANS(37))
        //         * SIN(RADIANS(langitude)) ) )) AS distance')
        //         )->get();


        // Mapper::map($gate->langitude, $gate->longitude, ['draggable' => false]);
        // Mapper::map(
        //     -6.200000, 106.816666,
        //     [
        //     'draggable' => true,
        //     'eventDragEnd' => 'test_js(this.getPosition().lat(), this.getPosition().lng());'
        //     ]);

        return view('gates.show', compact('gate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gates=Gate::findOrfail($id);
        $gates['distance']=DB::select('select * from public.distance()');

        $data['filterbranch'] = Branch::select('branch_code','branch_name')->get();
        //  dd($data['filterroad']);

        // $lat=DB::select('call distance(?)',array(8700)
        // );
        // $gates['distance']=$lat;
        // dd($gates['distance']);




        // Mapper::map(
        //     $gates->langitude, $gates->longitude,
        //     [
        //     'draggable' => true,
        //     'eventDragEnd' => 'test_js(this.getPosition().lat(), this.getPosition().lng());'
        //     ]);


        return view('gates.edit', compact('gates'),$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'gate_name'=>'required',
            'gate_code'=>'required',
            'list_adress'=>'required',
            'langitude'=>'required',
            'longitude'=>'required',
            'branch_info'=>'required',
            'radius'=>'required'
        ]);

        $gate=Gate::find($id);
        $gate->update($request->all());
        return redirect()->route('gates.index')->with('pesan', 'data berhasil diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\list  $list
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gate=Gate::find($id);
        $gate->delete();
        return redirect()->route('gates.index')->with('pesan', 'data berhasil dihapus');

    }


    public function branch_detail($id)
    {
        $data =  Branch::where('branch_id',$id)->get();
        // dd($data);

		return response()->json($data);
	}


}
