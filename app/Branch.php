<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
                        // 'branch_id',
                        'branch_code',
                        'branch_name'
                        // 'phone',
                        // 'email',
                        // 'address'
                        ];

}
