<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Virtual extends Model
{
    protected $fillable = ['vg_code','virtual_gantry','vg_type','gate_info','v_address','langitude','longitude','radius'];
}
