<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gate extends Model
{
    protected $fillable = ['branch_info','gate_name','gate_code','list_adress','langitude','longitude','radius'];
}
