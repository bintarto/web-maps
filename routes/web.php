<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Route::get('/', 'MapController@index');

//Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');

Route::resource('gates', 'ListsController');

Route::resource('branches', 'BranchController');

Route::resource('roads', 'RoadsController');

Route::resource('virtuals', 'VirtualController');

Route::resource('details', 'DetailController');


Route::get('/map', 'MapController@index');

Route::get('gates/branch_detail/{id}', 'ListsController@branch_detail')->name('branch_detail');
Route::get('details/gate_info/{id}', 'DetailController@gate_info')->name('gate_info');
Route::get('details/virtual/{id}', 'DetailController@virtual')->name('gate_code');
Route::get('details/gate_maps/{id}', 'DetailController@gate_maps')->name('gate_maps');







